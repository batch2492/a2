



// create student one
let studentOneName = "John";
let studentOneEmail = "john@mail.com";
let studentOneGrades = [89, 84, 78, 88];


// create student two
let studentTwoName = "Joe";
let studentTwoEmail = "joe@mail.com";
let studentTwoGrades = [78, 82, 79, 85];


// create student Three
let studentThreeName = "Jane";
let studentThreeEmail = "jane@mail.com";
let studentThreeGrades = [87, 89, 91, 93];

// create student Four
let studentFourName = "Jessie";
let studentFourEmail = "jessie@mail.com";
let studentFourGrades = [91, 89, 92, 93];



function login(email){
	console.log(`${email} has logged in`);
}

login(studentOneEmail);


function logout(email) {
	console.log(`${email} has logged out`);
}


function listGrades(grades){
	grades.forEach(grade => {
		console.log(grade);
	})
}


// use an object literal: {}

// Encapsulation - the organization of information (properties ) and behavior (as methods) to belong to the object that encapsulates them (the scope of encapsulation is denoted by the object literals)

let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],

	// add the functionalities available to a student as object methods

	login(){
		console.log(`${this.email} has logged in`);
	},
	logout(){
		console.log(`${this.email} has logged out`);
	},
	listGrades(){
		console.log(`${this.name} 's quartely grade averages are:  ${this.grades}`)
	},
	computeAve(){

		let sum = 0;
		this.grades.forEach(grade => sum += grade );
		return sum/this.grades.length;
	},

	willPass(){
		return this.computeAve() >= 85 ? true : false
	},

	willPassWithHonors(){
		return this.willPass() && this.computeAve() >= 90 ? true : undefined
	}
}


// Activity 

// Quiz 

/*
1. Spaghetti Code
2. Curly Braces
3. Encapsulation
4. studentOne.enroll()
5. True
6. let objectName = {
	property1 : data,
	property2 : data
	}

7. False
8. True
9. True
10.True

*/

// Function Coding 
// Number 1 
console.log("Number 1:");

let studentTwo = {
	name: 'Joe',
	email: 'joe@mail.com',
	grades: [78, 82, 79, 85],

	login(){
		console.log(`${this.email} has logged in`);
	},
	logout(){
		console.log(`${this.email} has logged out`);
	},
	listGrades(){
		console.log(`${this.name} 's quartely grade averages are:  ${this.grades}`)
	},
	computeAve(){

		let sum = 0;
		this.grades.forEach(grade => sum += grade );
		return sum/this.grades.length;
	},

	willPass(){
		return this.computeAve() >= 85 ? true : false
	},

	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : undefined
	}
}

let studentThree = {
	name: 'Jane',
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],

	login(){
		console.log(`${this.email} has logged in`);
	},
	logout(){
		console.log(`${this.email} has logged out`);
	},
	listGrades(){
		console.log(`${this.name} 's quartely grade averages are:  ${this.grades}`)
	},
	computeAve(){

		let sum = 0;
		this.grades.forEach(grade => sum += grade );
		return sum/this.grades.length;
	},

	willPass(){
		return this.computeAve() >= 85 ? true : false
	},

	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : undefined
	}
}

let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],

	login(){
		console.log(`${this.email} has logged in`);
	},
	logout(){
		console.log(`${this.email} has logged out`);
	},
	listGrades(){
		console.log(`${this.name} 's quartely grade averages are:  ${this.grades}`)
	},
	computeAve(){

		let sum = 0;
		this.grades.forEach(grade => sum += grade );
		return sum/this.grades.length;
	},

	willPass(){
		return this.computeAve() >= 85 ? true : false
	},

	willPassWithHonors(){
		return (this.willPass() && this.computeAve() >= 90) ? true : undefined
	}
}

console.log("studentOne");
console.log(studentOne);

console.log("studentTwo");
console.log(studentTwo);

console.log("studentThree");
console.log(studentThree);

console.log("studentFour");
console.log(studentFour);


// Number 2
console.log("Number 2");

console.log("studentOne.computeAve()");
console.log(studentOne.computeAve());

console.log("studentTwo.computeAve()");
console.log(studentTwo.computeAve());

console.log("studentThree.computeAve()");
console.log(studentThree.computeAve());

console.log("studentFour.computeAve()");
console.log(studentFour.computeAve());


// Number 3

console.log("Number 3");

console.log("studentOne.willPass()");
console.log(studentOne.willPass());

console.log("studentTwo.willPass()");
console.log(studentTwo.willPass());

console.log("studentThree.willPass()");
console.log(studentThree.willPass());

console.log("studentFour.willPass()");
console.log(studentFour.willPass());


// Number 4

console.log("Number 4");

console.log("studentOne.willPassWithHonors()");
console.log(studentOne.willPassWithHonors());

console.log("studentTwo.willPassWithHonors()");
console.log(studentTwo.willPassWithHonors());

console.log("studentThree.willPassWithHonors()");
console.log(studentThree.willPassWithHonors());

console.log("studentFour.willPassWithHonors()");
console.log(studentFour.willPassWithHonors());

//  Number 5

console.log("Number 5");

let classOf1A = {  

	students: [studentOne, studentTwo, studentThree, studentFour],

	countHonorStudents(){
		let good = 0;
		this.students.forEach(honor => (honor.willPassWithHonors()) ? good++ : false )
		return good;
	},

	honorsPercentage(){
		return this.countHonorStudents() / this.students.length * 100 
	},

	retrieveHonorStudentInfo(){
		let honors = [];
		this.students.forEach(honor => (honor.willPassWithHonors()) ? honors.push({email:honor.email, aveGrade: honor.computeAve()}) : false )
		return honors
		
	},

	sortHonorStudentsByGradeDesc(){
		let honors = [];
		this.students.forEach(honor => (honor.willPassWithHonors()) ? honors.push({email:honor.email, aveGrade: honor.computeAve()}) : false )
		return honors.sort(function(a, b){return b.aveGrade - a.aveGrade})
	}
}

console.log("classOf1A");
console.log(classOf1A);


//  Number 6

console.log("Number 6");

console.log("classOf1A.countHonorStudents()");
console.log(classOf1A.countHonorStudents());

//  Number 7

console.log("Number 7");

console.log("classOf1A.honorsPercentage()");
console.log(classOf1A.honorsPercentage());

//  Number 8

console.log("Number 8");

console.log("classOf1A.retrieveHonorStudentInfo()");
console.log(classOf1A.retrieveHonorStudentInfo());


//  Number 9

console.log("Number 9");

console.log("classOf1A.sortHonorStudentsByGradeDesc()");
console.log(classOf1A.sortHonorStudentsByGradeDesc());